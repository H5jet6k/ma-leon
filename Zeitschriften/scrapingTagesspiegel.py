# scraping basiert auf https://benedict-witzenberger.de/wordpress/2018/04/17/webscraping-
# in-python-3-wie-ich-es-mache/from distutils.log import error
from bs4 import BeautifulSoup
import urllib.request
from tqdm import tqdm
from urllib import request
 

# Grundaufbau von https://stackoverflow.com/questions/3075550/how-can-i-get-href-Link-from-html-using-python
def webseitenHolen(url):
    """
    Holt von der Autoren Webseite alle Webseiten und gibt sie in einem String zurück

    @param url: Die url der Website des Autors
    @returns: Eine Liste in welcher alle Webseiten-urls von von diesem Autor sind
    
    """
    # wird benötigt, da der Tagespiegel normale reuest blockiert
    req = urllib.request.Request(
        url, 
        data=None, 
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    soup = BeautifulSoup(request.urlopen(req).read())
    # unter ('ul', {'class' : 'hcf-teaser-list'}) sind alle artikel auffindbar
    absatzliste = soup.find('ul', {'class' : 'hcf-teaser-list'}) 
    artikelUrlsListe=[]
    for link in absatzliste.findAll('a'):
        # unter href findet man alle URLs
        artikelLink=link.get('href')
        artikelUrlsListe.append(artikelLink)
    # löscht mehrfach vorkommende URLs
    artikelUrlsListe = list(dict.fromkeys(artikelUrlsListe))
    return(artikelUrlsListe)
    

def webseiteZuText(url, autor):
    ''''
    Holt von den Webseiten den text und gibt ihn als str zurück

    @parm url: Ein Artikel
    @parm Autor: Den Namen des Autors der vermutlich diesen Artikel geschriben hat
    @returns: Ein den Artikel Text als str
    '''

    # da der Tagesspiegel normale Requests blockiert
    req = urllib.request.Request(
        url, 
        data=None, 
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    soup = BeautifulSoup(request.urlopen(req).read())

    # unter ('a', {'class' : 'ts-author ts-link'}) findet man Informationen zum Autor
    # wird genutzt um zu kontrollieren ob es sich um einen Artikel von diesem handelt
    textAutor = soup.find('a', {'class' : 'ts-author ts-link'})
    if autor in (textAutor.get_text()):
        
        # unter ('div', {'class' : 'ts-article-body'}) findet man den gesamten Text mit anzeigen usw.
        # die einzelnen Paragraphen sind jedoch nicht einheitlich gegenzeichnet
        htmlText = soup.find('div', {'class' : 'ts-article-body'}) 
        text = htmlText.get_text()

        # um die anzeigen ausschliessen zu können werden sie am Ende 'subtrahiert'
        # dazu mache ich mir zunutze, dass diese einheitlich gegenzeichnet sind
        werbung=[]
        for einschub in soup.find_all('aside', {'class' : 'newsletter'}):
            werbung.append(einschub.get_text())

        for einschub in soup.find_all('section', {'class' : 'sc-gSAPjG cIMknD tc'}):
            werbung.append(einschub.get_text())

        for einschub in soup.find_all('ul'):
            werbung.append(einschub.get_text())

        for einschub in soup.find_all('article', {'class' : 'ts-teaser ts-type-article'}):
            werbung.append(einschub.get_text())

        # folgende Dinge tauchen auch teilweise auf
        werbung.append('  ')
        werbung.append('Lesen Sie auch auf Tagesspiegel Plus:')
        werbung.append('Mehr zum Thema')

        # nun werden die Unterbrechungen durch split() gelöst und die
        # nun entstandenen abschnitte wieder zusammengeführt
        for unterbrechungen in list(dict.fromkeys(werbung)):
            abschnitte = text.split(unterbrechungen)
            ausgabe=''
            for fragment in abschnitte:
                ausgabe = ausgabe+str(fragment)
            # kann durch das Trennen und zusammen führen endstehen
            text = ausgabe.replace('\n','')
    return(text)
 

def save(liste,listenname, dateiname):
    import json

    datensammlung = {}
    datensammlung[listenname] = liste

    with open(dateiname, 'w') as outfile:
        json.dump(datensammlung, outfile)

'-------------------------------------------------------------------------------------'

urlAnfang ='https://www.tagesspiegel.de/suchergebnis/artikel/?p9049616='
urlEnde='h&sw=Thorsten+Mumme&search-ressort=2876'
autorName = 'Thorsten Mumme'
anzahlSeiten = 25

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'

speicherOrdner = pfad + 'Autoren/' + str(autorName)
 

urlsEnden=[]
# tqdm dient der Übersicht beim Ausführen
for durchlauf in tqdm(range(1,anzahlSeiten + 1), desc="Loading (1/3) ..."):
    urlsEnden=urlsEnden+webseitenHolen(urlAnfang  + str(durchlauf) + urlEnde)
# seltsamer weise wird hier nicht der ganze link ausgegeben, sondern nur das ende
# dies wird durch Anfügung an die Tagesspiegel Webseite behoben
urls=[]
for linkFragment in tqdm(urlsEnden, desc='Loading (2/3) ...'):
    urls.append('https://www.tagesspiegel.de/' + linkFragment)

# textZahl wird zur Überprüfungen benötigt
textZahl=0
for url in tqdm(urls, desc="Loading (3/3) ..."):
    try:
        text=webseiteZuText(url, autorName)
        # text wäre None wenn die Argumente nicht in der soup wären, könnte durch neues format der Webseite 
        # oder eine Falsche Webseite endstehen oder durch falschen Autor
        if text is not None:
            save(str(text),'text',speicherOrdner + '/' + str(textZahl) + '.txt')
            textZahl=textZahl+1
    except:
        pass
# ddurch sieht mann wie viele texte verlohren gehen
print('('+str(textZahl)+'/'+str(len(urlsEnden))+')')
