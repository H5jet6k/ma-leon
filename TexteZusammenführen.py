from tqdm import tqdm
import shutil
 

def texteZusammenführen(anzahlTexte, path, autoren, testTextAnzahl = 10):
    '''
    führt die Texte zusammen und teilt die Daten in Trainings und Testdaten

    @param anzahlTexte: wie viele Texte jeweils genommen werden sollen
    @param path: der Ort an welchem die Daten gespeichert sind
    @param testTextAnzahl: wie viele Texte als Test verwendet werden sollen
    
    '''
    # tqdm gibt einen Fortschrittsbalken an  
    for autor in tqdm(autoren, desc="Loading .."):
        zähler = 0
        # Um rutscht ins ty wen von diesem Autor keine Texte mehr existieren, da er weniger als die maximale Anzahl Texte besitzt
        try:
            for textzahl in range(0, anzahlTexte):
                # die ersten Texte werden als Testdaten verwendet
                if zähler < testTextAnzahl:
                    speicherOrt = path + 'AutorenDaten/test/' + autor + '/' + str(textzahl) + '.txt'
                    ladeOrt = path + 'Autoren/' + autor + '/' + str(textzahl) + '.txt'
                    shutil.copyfile(ladeOrt, speicherOrt)
                    
                else:
                    speicherOrt = path + 'AutorenDaten/test/' + autor + '/' + str(textzahl-testTextAnzahl) + '.txt'
                    ladeOrt = path + 'Autoren/' + autor + '/' + str(textzahl) + '.txt'
                    shutil.copyfile(ladeOrt, speicherOrt)
                zähler = zähler + 1
        except:
            pass

'-------------------------------------------------------------------------------------'

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'
autoren = ['Sven Clausen', 'Jan Dams', 'Markus Frühauf', 'Steffen Klusmann',  'Thorsten Mumme']
# Es sollten von allen Autoren ungefähr gleichviele Texte vorhanden sein, 
# um zu verhindern, dass der DL lehrt, dass das Ergebnis  zu 90% von einem Autor stammt
anzahlTexte = 200

texteZusammenführen(anzahlTexte, pfad, autoren)