# scraping basiert auf https://benedict-witzenberger.de/wordpress/2018/04/17/webscraping-
# in-python-3-wie-ich-es-mache/from distutils.log import error
from bs4 import BeautifulSoup
import urllib.request
from tqdm import tqdm
from urllib import request


#grund Aufbau von https://stackoverflow.com/questions/3075550/how-can-i-get-href-Link-from-html-using-python
def webseitenHolen(url):
    """
    Holt von der autoren Webseite alle Webseiten und gibt sie in einem String zurück

    @param url: Die url der Website des autors
    @returns: Eine Liste in welcher alle Webseiten-urls von von diesem autor sind
    
    """
    # wird benötigt, da die Welt normale reuest blockiert
    req = urllib.request.Request(
        url, 
        data=None, 
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
        }
    )
    soup = BeautifulSoup(request.urlopen(req).read())
    # der find Parameter wurde durch Untersuchen der Webseite herausgefunden
    # darunter sind alle Artikel gelistet
    soup = soup.find('ul', {'class' : 'c-grid c-grid--has-odd-item-count c-grid--has-three-items-in-last-row'})
    alpahLinks=[]
    # dadurch werden alle Artikel in eine Liste gepackt
    # hierbei sind jedoch auch Artikel endhalten welche Podcasts sind
    for artikel in soup.find_all('div', {'class' : 'c-dreifaltigkeit c-teaser-default__dreifaltigkeit'}):
        for link in artikel.find_all('a'):
            artikelLink=str(link.get('href'))
            # seltsamerweise erhält man nur die URL enden 
            alpahLinks.append('https://www.welt.de'+artikelLink)
    alpahLinks=list(dict.fromkeys(alpahLinks))
    ausgabeLinks = []
    for link in alpahLinks:
        # hier werden falsche Links welche keine Zeitung Artikel endhalten heraus gefiltert
        if 'podcasts' in link or 'video' in link or 'plus' in link:
            pass
        # korrekte zur liste hinzugefügt
        elif 'article' in link:
            ausgabeLinks.append(link)
        #es gibt links welche zu einer Webseite weiterleiten 
        # und diese auf welcher man einen button drücken muss, um beim korrekten Artikel zu landen
        # dies wird hier gemacht
        else:
            try:
                soup = BeautifulSoup(request.urlopen(link).read())
                artikel = soup.find('h4', {'class' : 'o-teaser__title c-teaser-default__title'})
                artikelLink = str(artikel.find('a'))
                if 'podcasts' not in artikelLink and 'video' not in artikelLink and 'plus' not in artikelLink:
                    ausgabeLinks.append('https://www.welt.de' + artikelLink)
            except:
                pass
    return(ausgabeLinks)
    

def webseiteZuText(url,autor):
    ''''
    Holt von den Webseiten den text und gibt ihn als str zurück

    @parm url: Ein Artikel
    @parm autor: Name des Artikel autors
    @returns: Ein den Artikel Text als str
    '''
    soup = BeautifulSoup(request.urlopen(url).read())
    # findargument wurde durch Untersuchen der Webseite gefunden
    textAutor=soup.find('span', {'class' : 'c-author__by-line'}).text
    if autor in textAutor:

        text = soup.find('div', {'class' : 'c-article-text c-content-container __margin-bottom--is-0'}).text
        
        # um die anzeigen ausschliessen zu können werden sie am Ende 'subtrahiert', 
        # dazu mache ich mir zunutze, 
        # dass diese (im Gegensatz zu den Artikeln) einheitlich gegenzeichnet sind
        werbung=[]
        for einschub in soup.find_all('div', {'data-qa' : 'InlineTeaser'}):
            werbung.append(einschub.get_text())

        for einschub in soup.find_all('span', {'class' : 'c-ad-appnexus__label'}):
            werbung.append(einschub.get_text())

        for einschub in soup.find_all('div', {'class' : 'c-inline-element'}):
            werbung.append(einschub.get_text())
        
        # taucht manchmal durch ausschneiden von anderen Elementen auf
        werbung.append('  ')

        # nun werden die Unterbrechungen durch Split gelöst und die
        # nun entstandenen abschnitte wieder zusammengeführt
        for unterbrechungen in list(dict.fromkeys(werbung)):
            abschnitte = text.split(unterbrechungen)
            ausgabe=''
            for fragment in abschnitte:
                ausgabe = ausgabe+str(fragment)
            text = ausgabe
    return(text)

def save(liste,listenname, dateiname):
    import json

    datensammlung = {}
    datensammlung[listenname] = liste

    with open(dateiname, 'w') as outfile:
        json.dump(datensammlung, outfile)

'-------------------------------------------------------------------------------------'

url ='https://www.welt.de/autor/jan-dams/'
autor = 'Jan Dams'

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'

speicherOrdner = pfad + 'Autoren/' + str(autor)

link=webseitenHolen(url)
# dient der Fehler Überprüfung
textZahl=0
# tqdm dient lediglich der Fortschritt überwachung nach dem Ausführenden 
for url in tqdm(link, desc="Loading ..."):
    try:
        text=webseiteZuText(url, autor)
        # wäre leer wenn Autor falsch ist
        if text is not None:
            save(str(text).strip(),'text',speicherOrdner + '/' + str(textZahl) + '.txt')
            textZahl=textZahl+1
    except:
        pass
print('('+str(textZahl)+'/'+str(len(link))+')')
