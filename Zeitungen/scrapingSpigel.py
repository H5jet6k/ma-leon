# scraping basiert auf https://benedict-witzenberger.de/wordpress/2018/04/17/webscraping-
# in-python-3-wie-ich-es-mache/
from bs4 import BeautifulSoup
from tqdm import tqdm
from urllib import request
 

# Grundaufbau von https://stackoverflow.com/questions/3075550/how-can-i-get-href-Link-from-html-using-python
def webseitenHolen(url):
    """
    Holt von der Autoren Webseite alle Webseiten und gibt sie in einem String zurück

    @param url: Die URL der Website des Autors
    @returns: Eine Liste in welcher alle Webseiten-URLs von diesem Autor sind
    
    """
    soup = BeautifulSoup(request.urlopen(url).read())
    # ('section', {'data-area' : 'article-teaser-list'}) wurde durch untersuchen der
    #  Webseite per Hand herausgefunden und alle Artikel werden darunter gefunden
    absatzliste =  soup.find('section', {'data-area' : 'article-teaser-list'})
    artikelUrlsListe=[]
    for link in absatzliste.findAll('a'):
        artikelUrl=str(link.get('href'))
        # manager-magazin ist ein Magazin von Spiegel welches jedoch gleich aufgebaut ist wodurch 
        # Folgende Schritte für beide funktionieren
        if 'www.spiegel.de/' in artikelUrl or 'www.manager-magazin.de/' in artikelUrl:
            artikelUrlsListe.append(artikelUrl)
    # löscht mehrfach vorkommende URLs
    artikelUrlsListe=list(dict.fromkeys(artikelUrlsListe))
    return(artikelUrlsListe)
 

def webseiteZuText(url, autor):
    ''''
    Holt von den Webseiten den Text und gibt ihn als str zurück

    @parm url: Ein Artikel
    @parm autor: Name des Artikel Autors
    @returns: Ein den Artikel Text als str
    '''

    soup = BeautifulSoup(request.urlopen(url).read())
    autorName, chefredakteurName = '', ''
    try:
        # unter ('div', {'class' : 
        # 'font-sansUI lg:text-base md:text-base sm:text-s text-shade-dark dark:text-shade-light mb-4'}) 
        # findet man den Autor eines Tests (kann auf der Webseite durch F11 herausgefunden werden)
        autorName= soup.find('div', {'class' : 
            'font-sansUI lg:text-base md:text-base sm:text-s text-shade-dark dark:text-shade-light mb-4'}).text
    except:
        # manchmal findet man beim cheffredakteur auch einen anderen text dazu dies
        chefredakteurName = soup.find('div', {'class' : 
        'font-serifUI lg:text-xl md:text-xl sm:text-l italic mb-2'}).text
    if autor in autorName or autor in chefredakteurName: 
        # unter ('div', {'data-area' : 'body'}) findet man den gesamten Artikel Text
        webText = soup.find('div', {'data-area' : 'body'})
        ausgabeText=''
        # unter ('div', {'data-sara-click-el' : 'body_element'}) findet man die Paragraphen 
        # welche zum Text gefören, dadurch werden Anzeigen und Empfehlungen ausgeschlossen
        for textabsatz in webText.findAll('div', {'class' : 
            'RichText lg:w-8/12 md:w-10/12 lg:mx-auto md:mx-auto lg:px-24 md:px-24 sm:px-16 break-words word-wrap'}):
            # '\' und '  ' entstehen teilweise beim zusammenführen 
            text = textabsatz.text.replace('\n','').replace('  ','')
            ausgabeText = ausgabeText + str(text)
        return(ausgabeText)
    else:
        return(None)
 

def save(liste,listenname, dateiname):
    import json

    datensammlung = {}
    datensammlung[listenname] = liste

    with open(dateiname, 'w') as outfile:
        json.dump(datensammlung, outfile)

'-------------------------------------------------------------------------------------'

url ='https://www.manager-magazin.de/impressum/autor-2736cf22-0001-0003-0000-000000019134'
autor = 'Sven Clausen'
anzahlSeiten = 21

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'

speicherOrdner = pfad + 'Autoren/' + str(autor)
 

urls=webseitenHolen(url)
#tqdm dient lediglich der fortschritt Überwachung nach dem Ausführen
for durchlauf in tqdm(range(1,anzahlSeiten+1), desc="Loading (1/2) ..."):
    urls = urls+webseitenHolen(url+'/p'+str(durchlauf))
 
#dient der Fehler Überprüfung 
textZahl=0
for url in tqdm(urls, desc="Loading (2/2) ..."):
    try:
        text=webseiteZuText(url, autor)
        # wäre leer, wenn Autor Falsch ist
        if text is not '' and text is not None:
            save(text,'text',speicherOrdner + '/' + str(textZahl) + '.txt')
            textZahl=textZahl+1
    except:
        # dadurch können generelle fehler überprüft werden sollte normalerweise nicht ausgelösst werden
        print(url)
# solten texte nicht verarbeitet werden können sieht man dies hier
# normalerweise sind über 50% der Artikel nuzbar
print('('+str(textZahl)+'/'+str(len(urls))+')')
