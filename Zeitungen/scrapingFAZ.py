# scraping basiert auf https://benedict-witzenberger.de/wordpress/2018/04/17/webscraping-
# in-python-3-wie-ich-es-mache/from distutils.log import error
from bs4 import BeautifulSoup
from tqdm import tqdm
from urllib import request
 

# Grundaufbau von https://stackoverflow.com/questions/3075550/how-can-i-get-href-Link-from-html-using-python
def webseitenHolen(url):
    """
    Holt von der Autoren Webseite alle Webseiten und gibt sie in einem String zurück

    @param url: Die url der Website des Autors
    @returns: Eine Liste in welcher alle Webseiten-urls von von diesem Autor sind
    
    """
    soup = BeautifulSoup(request.urlopen(url).read())

    # (ul', {'class' : 'lst-Teaser'?}) ist nur für FAZ gültig 
    # und wurde durch Untersuchen der Webseite per Hand herausgefunden 
    # alle Artikel werden darunter gefunden
    absatzliste = soup.find('ul', {'class' : 'lst-Teaser'}) 
    artikelUrlsListe=[]
    for link in absatzliste.findAll('li', {'class', 'lst-Teaser_Item'}):
        # holt alle URLs
        artikelUrl=link.get('href')
        artikelUrlsListe.append(artikelUrl)
    # löscht mehrfach vorkommende URLs
    artikelUrlsListe=list(dict.fromkeys(artikelUrlsListe))
    return(artikelUrlsListe)
     

def webseiteZuText(url, autor):
    ''''
    Holt von den webseiten den Text und gibt ihn als str zurück

    @parm url: Ein Artikel
    @parm Autor: Den Namen des Autors der vermutlich diesen Artikel geschrieben hat
    @returns: Ein den Artikel Text als str
    '''
    soup = BeautifulSoup(request.urlopen(url).read())

    # ('a', {'class' : 'atc-MetaAuthorLink'}) ist nur für FAZ gültig 
    # und wurde durch Untersuchen der Webseite per Hand herausgefunden 
    # darunter findet man den Autor
    autorenAbschnitt = soup.find('a', {'class' : 'atc-MetaAuthorLink'}).get_text()
    if autor in autorenAbschnitt:

    # ('div', {'class' : 'atc-Text'}) ist nur für FAZ gültig 
    # und wurde durch Untersuchen der Webseite per Hand herausgefunden 
    # darunter ist der ganze Artikel, jedoch auch mit Anzeigen, welche herausgefiltert werden sollten
        webText = soup.find('div', {'class' : 'atc-Text'}) 

        # ('p', {'class' : 'First atc-TextParagraph'}) ist der erste Paragraph des Artikels
        ausgabeText = webText.find('p', {'class' : 'First atc-TextParagraph'}).get_text()
        # Alle folgenden Paragraphen werden mit ('p', {'class' : 'atc-TextParagraph'}) bezeichnet
        for Absatz in webText.find_all('p', {'class' : 'atc-TextParagraph'}):
            ausgabeText = ausgabeText + Absatz.get_text()
        # wird benötigt zur Normalisierung
        ausgabeText = ausgabeText.replace('\n','')

        return(ausgabeText)
 

def save(liste,listenname, dateiname):
    import json

    datensammlung = {}
    datensammlung[listenname] = liste

    with open(dateiname, 'w') as outfile:
        json.dump(datensammlung, outfile)

'-------------------------------------------------------------------------------------'

urlAnfang ='https://www.faz.net/redaktion/markus-fruehauf-11104557.html'
urlEnde='#authorPagination'
autorName = 'Markus Frühauf'
anzahlSeiten = 174

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'

speicherOrdner = pfad + 'Autoren/' + str(autorName)


# Zusammensetzung der URL aufgrund, dass die einzelnen Seiten auf 
# welchen die die Artikel sind, einen unterschiedlichen Mittelteil besitzen 
urls = webseitenHolen(urlAnfang + urlEnde)
# tpdm dient rein der Übersichtlichkeit nach ausführen und zeigt vorschritt an
for durchlauf in tqdm(range(1, anzahlSeiten + 1), desc="Loading (1/2) ..."):
    # ?offset= ist der Mittelteil ergänzt durch eine Zahl (den durchlauf)
    urls = urls + webseitenHolen(urlAnfang + '?offset=' + str(durchlauf) + urlEnde)
 

textZähler=0 # dient lediglich der kontrolle
for url in tqdm(urls, desc="Loading (2/2) ..."):
    try:
        text = webseiteZuText(url, autorName)
        # Text wäre None wenn die Argumente nicht in der soup wären 
        # könnte durch neues Format der Webseite oder eine Falsche Webseite endstehen
        # oder sollte der Autor nicht stimmen
        if text is not None:
            save(str(text),'text',speicherOrdner + '/' + str(textZähler) + '.txt')
            textZähler=textZähler+1
    except:
        pass
# durch sieht man wie viele texte verloren gehen
print('('+str(textZähler)+'/'+str(len(urls))+')')