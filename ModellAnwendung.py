from tensorflow.keras import layers
from tensorflow.keras import losses 
import re
import string
import tensorflow as tf

 

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'

 

def vectorizeLayerErstellen(batch_size, max_features, path, seed=42):
    """
    Erstellt einen Vectorize Layer aus den Daten

    @param batch_size: losgröße: Stichprobe mit der das Netzwerk gleichzeitig trainiert wird
    @param max_features: maximale Merkmale (Tokens): Anzahl Elemente (meist Worte)
    @param path: der Pfad wo die Daten zu finden sind
    @param seed: ein ausgangswert, von welchem 'Zufall' errechnet wird, 
                 wird derselbe verwendet erhält man dasselbe Ergebnis
    @returns vectorize_layer: Eine Art Index, welcher die Tokenisierte Bibliothek enthält
    @returns raw_train_ds: der rohe Trainings Datensatz 
    
    """
 
    'Daten aufnehmen'
    folder_name = path + 'AutorenDaten'
    raw_train_ds = tf.keras.utils.text_dataset_from_directory(
        folder_name + '/train', 
        batch_size=batch_size, 
        validation_split=0.2, 
        subset='training', 
        seed=seed)

    raw_val_ds = tf.keras.utils.text_dataset_from_directory(
        folder_name + '/train', 
        batch_size=batch_size, 
        validation_split=0.2, 
        subset='validation', 
        seed=seed)

    raw_test_ds = tf.keras.utils.text_dataset_from_directory(
        folder_name + '/test', 
        batch_size=batch_size)

 

    def custom_standardization(input_data):
        lowercase = tf.strings.lower(input_data)
        stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
        return tf.strings.regex_replace(stripped_html,
                                        '[%s]' % re.escape(string.punctuation),
                                        '')
 

    sequence_length = 250
    vectorize_layer = layers.TextVectorization(
        standardize=custom_standardization,
        max_tokens=max_features,
        output_mode='int',
        output_sequence_length=sequence_length)

    # Make a text-only dataset (without labels), then call adapt
    train_text = raw_train_ds.map(lambda x, y: x)
    vectorize_layer.adapt(train_text)

    def vectorize_text(text, label):
        text = tf.expand_dims(text, -1)
        return vectorize_layer(text), label

    train_ds = raw_train_ds.map(vectorize_text)
    AUTOTUNE = tf.data.AUTOTUNE
    train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)

    return(vectorize_layer, raw_train_ds)

 
def laden(dateiname):
    import json
    f = open(dateiname)
    ausgabe = json.load(f)
    return(ausgabe)
 

def listenLaden(dateiname,listennaheme = 'list'):
    import json
    with open(dateiname) as datei:
        datensammlung = json.load(datei)
        return datensammlung[listennaheme]

'----------------------------------------------------------------------------------------'

'Modell holen'
werte = listenLaden(pfad + 'Modell/modelb/werte.txt')
max_features, epochse, batch_size = werte[0], werte[3], werte[4]
 
 
vectorize_layer, raw_train_ds = vectorizeLayerErstellen(batch_size, max_features, pfad)

modellSpeicherOrt = pfad + 'Modell/model5' 
model=tf.keras.models.load_model(modellSpeicherOrt)
export_model = tf.keras.Sequential([
    vectorize_layer,
    model,
    layers.Activation('sigmoid')
])

export_model.compile(
    loss=losses.BinaryCrossentropy(from_logits=False), optimizer="adam", metrics=['accuracy']
)

export_model.compile(optimizer='sgd',
            loss=tf.keras.losses.SparseCategoricalCrossentropy(),
            metrics=['accuracy'])
 
 
'Modell Anwenden'

beispiele =[]
for textZahlen in range(1,6):
    text = pfad + 'Test/' + str(textZahlen) + '.txt'
    beispiele.append(str(laden(text)))

print("Label 0 corresponds to", raw_train_ds.class_names[0])
print("Label 1 corresponds to", raw_train_ds.class_names[1])
print("Label 2 corresponds to", raw_train_ds.class_names[2])
print("Label 3 corresponds to", raw_train_ds.class_names[3])
print("Label 4 corresponds to", raw_train_ds.class_names[4])

ergebnise = export_model.predict(beispiele)
print(ergebnise)
 

for ergebnis in ergebnise:
    listeErgebnis = []
    for wert in ergebnis:
        listeErgebnis.append(wert)
    autorenLabel = listeErgebnis.index(max(listeErgebnis))
    autorenName = raw_train_ds.class_names[autorenLabel]
    print(autorenName)