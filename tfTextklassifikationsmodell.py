from tensorflow.keras import layers
from tensorflow.keras import losses 
import matplotlib.pyplot as plt
import re
import string
import tensorflow as tf
import time
  

def laden(dateiname,listennaheme):
    import json
    with open(dateiname) as datei:
        datensammlung = json.load(datei)
        return datensammlung[listennaheme]
 

def save(liste,listenname, dateiname):
    import json

    datensammlung = {}
    datensammlung[listenname] = liste

    with open(dateiname, 'w') as outfile:
        json.dump(datensammlung, outfile)

'-----------------------------------------------------------------------------------------'

pfad = '/Users/schule/Documents/Informatik/VS-MA/DATEN/'
# maximaleMerkmale: Anzahl Elemente (meist Worte) (Tokens)
# einbettungsdim: Dimension in welcher die Eingabe werte dargestellt werden (Faustregel: (Anzahl eindeutigen kategorialen Elemente) ^1/4)
# dropout: Mutation Chance 
# losgröße: Stichprobe mit der das Netzwerk gleichzeitig trainiert wird des do grösser, des do Platzintensiver und genauer.
maximaleMerkmale, einbettungsdim, dropout, epochen, losgröße = 50000, 2**8, 0.2, 15000, 10000

start=time.time()

 

max_features, embedding_dim, Dropout, epochse, batch_size = maximaleMerkmale, einbettungsdim, dropout, epochen, losgröße
folder_name = pfad + 'AutorenDaten'
seed = 42

'''
Vectorize Layer Erstellen
'''
'Daten aufnehmen'
raw_train_ds = tf.keras.utils.text_dataset_from_directory(
    folder_name + '/train', 
    batch_size=batch_size, 
    validation_split=0.2, 
    subset='training', 
    seed=seed)

raw_val_ds = tf.keras.utils.text_dataset_from_directory(
    folder_name + '/train', 
    batch_size=batch_size, 
    validation_split=0.2, 
    subset='validation', 
    seed=seed)

raw_test_ds = tf.keras.utils.text_dataset_from_directory(
    folder_name + '/test', 
    batch_size=batch_size)

  

def custom_standardization(input_data):
  lowercase = tf.strings.lower(input_data)
  stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
  return tf.strings.regex_replace(stripped_html,
                                  '[%s]' % re.escape(string.punctuation),
                                  '')
 

sequence_length = 250
vectorize_layer = layers.TextVectorization(
    standardize=custom_standardization,
    max_tokens=max_features,
    output_mode='int',
    output_sequence_length=sequence_length)

# Make a text-only dataset (without labels), then call adapt
train_text = raw_train_ds.map(lambda x, y: x)
vectorize_layer.adapt(train_text)

def vectorize_text(text, label):
  text = tf.expand_dims(text, -1)
  return vectorize_layer(text), label

train_ds = raw_train_ds.map(vectorize_text)
val_ds = raw_val_ds.map(vectorize_text)
test_ds = raw_test_ds.map(vectorize_text)

AUTOTUNE = tf.data.AUTOTUNE

train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

'''
Modell Trainieren
'''
'Modell erstellen'
# Anstelle hiervon kann auch ein alte Modell geladen werden um es weiter zu trainieren. (112-113)
model = tf.keras.Sequential([
  layers.Embedding(max_features + 1, embedding_dim),
  layers.Dropout(Dropout),
  layers.GlobalAveragePooling1D(),
  layers.Dropout(Dropout),
  layers.Dense(5)]) #angepast von 1 auf 5 wegen multiclass


'''modellSpeicherOrt = pfad + 'Modell/model' 
model=tf.keras.models.load_model(modellSpeicherOrt)'''

'Modell trainieren'
model.compile(optimizer='sgd',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(),
              metrics=['accuracy'])

history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochse)

'Modell testen'
loss, accuracy = model.evaluate(test_ds)

print("Loss: ", loss)
print("Accuracy: ", accuracy)

history_dict = history.history
history_dict.keys()

acc = history_dict['accuracy']
val_acc = history_dict['val_accuracy']
loss = history_dict['loss']
val_loss = history_dict['val_loss']

epochs = range(1, len(acc) + 1)

# "bo" is for "blue dot"
plt.plot(epochs, loss, 'bo', label='Training loss')
# b is for "solid blue line"
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()
 

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend(loc='lower right')

plt.show()
 

'Modell Exportieren'

export_model = tf.keras.Sequential([
  vectorize_layer,
  model,
  layers.Activation('sigmoid')
])

export_model.compile(
    loss=losses.BinaryCrossentropy(from_logits=False), optimizer="adam", metrics=['accuracy']
)

export_model.compile(optimizer='sgd',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(),
              metrics=['accuracy'])

# Test it with `raw_test_ds`, which yields raw strings
loss, accuracy = export_model.evaluate(raw_test_ds)
print(accuracy)

'Speicherung'

weg = pfad + 'Modell'
model.save(weg + '/model5')

werte= [max_features, embedding_dim, Dropout, epochse, batch_size, accuracy, seed]
save(werte, 'list', weg + '/model5/werte.txt')

'Abschliessen'
print(werte)
print('time needed: ', (time.time()-start)/3600, ' hours')
